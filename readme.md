# Ejemplos LoRa - The Things Network
Este repositorio contiene el código fuente de las demostraciones realizadas en
el Lanzamiento de la Comunidad The Things Network para San Salvador.

Lee el archivo readme.md dentro de cada carpeta para obtener más información
sobre cómo conectarlos y hacerlos funcionar.

## Demos disponibles

1. [Detector de Humanos](./HumanDetector): Detecta cada vez que una persona
observa el sensor de frente.
1. [Transmisor de Temperatura y Humedad](./TemperaturaPresion): Captura los
valores de temperatura y humedad y los transmite con LoRaWAN.

## Recursos externos
[ESP32 LoRa Gateway - Componente para Fritzing](https://gitlab.com/hackerspacesv/fritzingparts/tree/master/ESP32LoRa1CHGateway): Archivo para importar pieza de la placa ESP32 LoRa Gateway.
