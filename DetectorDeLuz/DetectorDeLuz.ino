/*
 * El presente Sketch envía una notificación a The Things Network
 * cuando el valor recibido de luz es menor que el establecido por
 * el punto de corte. Puede ser utilizado para detectar presencia
 * de personas o como simple detector de iluminación P.E. Iluminación.
 */
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include "ttn_config.h"

// Valores constantes
const int pinAnalogo = A18;  // Pin donde se realiza la lectura del sensor
const int MUESTRAS = 100;    // Define la frecuencia de muestras análogas
const int INTERVALO = 20000; // Los mensajes se enviaran cada 20 segundos

// Valores de configuración de The Things Network
// No modificarlos aquí. Utilizar el archivo "ttn_config.h"
static const PROGMEM u1_t NWKSKEY[16] = TTN_NETWORK_SESSION_KEY;
static const u1_t PROGMEM APPSKEY[16] = TTN_APP_SESSION_KEY;
static const u4_t DEVADDR = TTN_DEVICE_ADDRESS; // <-- Change this address for every node!

// Variables utilizadas por la aplicacion:
int valorSensor = 0;        // Valor que almacena el valor del sensor análogo
long ultimaLectura = 0;     // Guarda el tiempo desde la última lectura analoga
long ultimoEnvio = 0;       // Guarda el tiempo desde el ultimo envio a TTN
bool sensorActivo = false;  // Guarda si el sensor se activó

// Variable que almacena los datos a enviar
uint8_t datos[5] = {0,0,0};

// "Tarea" que envía los datos sobre el aire
static osjob_t tarea_envio;

// Las siguientes funciones son solo llamadas en las activaciones
// sobre el aire. No se usan en este ejemplo, se incluyen solon para
// evitar errores del compilador.
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

// Configuración de pines para el Radio LoRa
const lmic_pinmap lmic_pins = {
  .nss = 16,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {26, 33, 32},
};

// Definición de funciones utilizadas por el cliente LoRa
void onEvent (ev_t ev);
void do_send(osjob_t* j);

void setup() {
  // La siguiente instrucción habilita el puerto serie a 115200 baudios
  Serial.begin(115200);

  // Utilizaremos el LED incluído para indicar que hay detección
  pinMode(LED_BUILTIN, OUTPUT);
  
  delay(100); // Se espera 100ms antes de iniciar el Radio
  Serial.println(F("Iniciando..."));

  // Las siguientes llamadas inicializan el Radio LoRa
  // y los parámetros de sesión para enviar datos a The Things Network
  os_init(); // Inicializaci[on de la librería
  LMIC_reset(); // Re-inicio de la MAC
  #ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x13, DEVADDR, nwkskey, appskey);
  #else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x13, DEVADDR, NWKSKEY, APPSKEY);
  #endif

  // El Salvador utiliza la misma configuración de región de Austrália
  #ifdef CFG_au921
  // Para The Things Network primero deshabilitamos todas las sub-bandas
  for(int b=0; b<8; ++b) {
    LMIC_disableSubBand(b);
  }
  // En caso de utilizar un Gateway multi-canal
  // Deshabilitamos todas las sub-bandas
  // y utilizamos únicamente la sub-banda 1
  #ifdef TTN_GW_MULTI
    LMIC_selectSubBand(1);
  #endif
  // En caso de utilizar un Gateway de canal único
  // debemos de seleccionar el canal correspondiente
  #ifdef TTN_GW_1CHANNEL
    LMIC_enableChannel(0); // Tiene que coincidir con el Gateway
  #endif
  #endif

  LMIC_setLinkCheckMode(0); // No verificamos el enlace

  // Dispersión de senal de las respuestas del gateway (TTN utiliza SF9)
  LMIC.dn2Dr = DR_SF9; 

  // Esta función establece la potencia y dispersión de la senal de salida
  // un SF mas alto garantiza mejor recepción pero menor cantidad de datos
  LMIC_setDrTxpow(DR_SF9,14);
}

void loop() {
  
  // Leemos el valor análogo
  int valorActual = analogRead(pinAnalogo);

  //Serial.print("Valor: ");
  //Serial.println(valorActual, DEC);

  if((millis()-ultimaLectura) >= MUESTRAS) {
    if(valorActual<valorSensor && valorActual<1000 && sensorActivo==false) {
      sensorActivo |= true;
      digitalWrite(LED_BUILTIN, HIGH); // Activamos el indicador
    }
    ultimaLectura = millis();
    valorSensor = valorActual;
  }

  // Si ya se cumplió el intervalo y el sensor está activo
  // Intentamos enviar el paquete.
  if((millis()-ultimoEnvio) >= INTERVALO && sensorActivo) {
    enviar(&tarea_envio);
    ultimoEnvio = millis();
  }

  os_runloop_once();
}

void onEvent(ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     Serial.println(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE: // Se llega aquí luego de enviar un paquete
            Serial.println(F("EV_TXCOMPLETE (incluye la espera por respuestas del gateway)"));
            sensorActivo = false; // Desactivamos el sensor.
            digitalWrite(LED_BUILTIN, LOW); // Activamos el indicador
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Recibido ACK"));
            if (LMIC.dataLen) {
              Serial.println(F("Recibido "));
              Serial.println(LMIC.dataLen);
              Serial.println(F(" bytes de carga"));
            }
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        /*
        || Este evento está definido pero no se
        || utiliza en el código se comenta para
        || no desperdiciar espacio de código en el.
        ||
        || case EV_SCAN_FOUND:
        ||    Serial.println(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        default:
            Serial.print(F("Evento desconocido: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void enviar(osjob_t* j) {
    // Verifica si no hay una operación de envío pendiente
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, no se envia. Ya hay paquete en cola"));
    } else {
        datos[0] = 0xCA;
        datos[1] = 0xFE;
        datos[2] = sensorActivo;
        LMIC_setTxData2(1, datos, 3, 0);
        Serial.println(F("Paquete en cola."));
    }
}
