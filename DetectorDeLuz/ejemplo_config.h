/* Importante: Antes de comenzar reemplaze aquí con los valores
 * obtenidos de la consola de The Things Network y renombre el
 * archivo a "ttn_config.h"
 */
#ifndef TTN_CONFIG
#define TTN_CONFIG

// Comente una de las siguientes lineas dependiendo del Gateway:
//#define TTN_GW_1CHANNEL // El gateway es de un solo canal
#define TTN_GW_MULTI // El gateway soporta multi-canal

// Clave de sesión de red 
#define TTN_NETWORK_SESSION_KEY { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// Clave de sesión de aplicación
#define TTN_APP_SESSION_KEY { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// Identificación del dispositivo
#define TTN_DEVICE_ADDRESS 0x00000000

#endif
