# Ejemplos LoRa - The Things Network
Esta es una demostración rápida realizada para el lanzamiento de la Comunidad
The Things Network de la Ciudad de San Salvador.

## Sensor de Temperatura y Humedad
Este demo envía a The Things Network los valores capturados de temperatura y
humedad por el sensor DHT03.

### Lista de Componentes
| Nombre                                                          | Cantidad |
|-----------------------------------------------------------------|----------|
|[Gateway Lora 1 Canal](https://www.sparkfun.com/products/15006)* | 1        |
| Adaptador u.FL a SMA                                            | 1        |
| Antena 915MHz con terminal SMA                                  | 1        |
|[Sensor RHT03](https://www.sparkfun.com/products/10167)          | 1        |
| Breadboard Mediana                                              | 1        |
| Cable Jumper (M/M)                                              | 3        |
| Cable Micro-USB                                                 | 1        |
| Cargador USB                                                    | 1        |

\* Puede ser reemplazado por una tarjeta basada en el ESP32 + un radio RFM95W.
Recuerda que configuraciones adicionales son requeridas para utilizar la banda
libre en El Salvador.

### Conexiones
Asegúrate de conectar el sensor de la siguiente manera.

| Gateway Lora | DHT03 |
|--------------|-------|
| GND          | PIN 4 |
| 3V3          | PIN 1 |
| PIN 19       | PIN 2 |

### Dependencias para Arduino
1. Asegurate de tener [soporte para el ESP32](https://github.com/espressif/arduino-esp32)
2. Instala la [libreria LMIC](https://github.com/matthijskooijman/arduino-lmic)
3. En el archivo *Arduino/libraries/arduino-lmic/project_config/lmic_project_config.h*
asegúrate de tener seleccionada la región correcta (Para El Salvador 
es *#define CFG_au921 1*)

### Sketch de Arduino
Utiliza el Sketch en la carpeta *TempPressureSensor*, solo asegúrate de colocar
las llaves de aplicación de tu dispositivo que se generan en la consola de The
Things Network.

### Formato para procesamiento de datos
Utiliza el siguiente formato para procesar la data en la consola de The Things
Network y poder extraer los valores capturados por el sensor.

    function Decoder(bytes, port) {
      // Este objeto contendrá todos los datos
      // resultado
      var obj_result = {};

      // Convertimos el resultado a una cadena
      // para poder depurar fácilmente
      var str = "";
      for(var index = 0; index < bytes.length; index += 1)
        str += String.fromCharCode(bytes[index]);
      obj_result.txt_msg = str;

      // Extraemos los valores de temperatura y humedad
      // con una expresión regular
      if(bytes.length == 12 && (String.fromCharCode(bytes[0])=="t")) {
        var matchResult = /t([0-9]{2}\.[0-9]{2})h([0-9]{2}\.[0-9]{2})/.exec(str);
        if(matchResult.length==3) {
          obj_result.temperature = parseFloat(matchResult[1]);
          obj_result.humidity = parseFloat(matchResult[2]);
        }
      }

      return obj_result;
    }