# Ejemplos LoRa - The Things Network
Esta es una demostración rápida realizada para el lanzamiento de la Comunidad
The Things Network de la Ciudad de San Salvador.

## Detector de Humanos
Este demo envía a The Things Network una señal cada vez que una persona observa
el sensor de frente.

### Lista de Componentes
| Nombre                                                          | Cantidad |
|-----------------------------------------------------------------|----------|
|[Gateway Lora 1 Canal](https://www.sparkfun.com/products/15006)* | 1        |
| Adaptador u.FL a SMA                                            | 1        |
| Antena 915MHz con terminal SMA                                  | 1        |
|[OpenMV M7](https://openmv.io/products/openmv-cam-m7)            | 1        |
| Breadboard Mediana                                              | 1        |
| Cable Jumper (M/M)                                              | 3        |
| Cable Micro-USB                                                 | 1        |
| Cargador USB                                                    | 1        |

\* Puede ser reemplazado por una tarjeta basada en el ESP32 + un radio RFM95W.
Recuerda que configuraciones adicionales son requeridas para utilizar la banda
libre en El Salvador.

### Conexiones
Asegúrate de conectar el sensor de la siguiente manera.

| Gateway Lora | OpenMV |
|--------------|--------|
| GND          | GND    |
| 3V3          | 3.3    |
| PIN 19       | PIN 4  |

### Dependencias para Arduino
1. Asegurate de tener [soporte para el ESP32](https://github.com/espressif/arduino-esp32)
2. Instala la [libreria LMIC](https://github.com/matthijskooijman/arduino-lmic)
3. En el archivo *Arduino/libraries/arduino-lmic/project_config/lmic_project_config.h*
asegúrate de tener seleccionada la región correcta (Para El Salvador 
es *#define CFG_au921 1*)

### Sketch de Arduino
Utiliza el Sketch en la carpeta *Transmitter*. Sólo asegúrate de colocar
las llaves de aplicación de tu dispositivo que se generan en la consola de The
Things Network.

### Programa para OpenMV
Utiliza el archivo *person_detection.py* en la carpeta OpenMV

### Formato para procesamiento de datos
Utiliza el siguiente formato para procesar la data en la consola de The Things
Network y poder extraer los valores capturados por el sensor.

    function Decoder(bytes, port) {
      // Este objeto contendrá todos los datos
      // resultado
      var obj_result = {};

      // Convertimos el resultado a una cadena
      // para poder depurar fácilmente
      var str = "";
      for(var index = 0; index < bytes.length; index += 1)
        str += String.fromCharCode(bytes[index]);
      obj_result.txt_msg = str;

      // Agregar una variable si el humano es detectado
      if(bytes.length == 2 && (String.fromCharCode(bytes[0])=="h")) {
        if(String.fromCharCode(bytes[1])=="1") {
          obj_result.human = true;
        }
        if(String.fromCharCode(bytes[1])=="0") {
          obj_result.human = false;
        }
      }

      return obj_result;
    }