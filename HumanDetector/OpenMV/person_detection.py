# Implementación basada en el ejemplo de detección de caras incluído en OpenMV
#
# Nota: Originalmente se enviaría el número de personas frente a la cámara vía
# puerto serie. Pero la mayoría de pines están ocupados en la tarjeta de
# expansión de OpenMV. Se decidió utilizar un detector sencillo que controla
# el nivel de voltaje en el pin 4 y leer el nivel del lado de la tarjeta LoRa.

# Importar pyb para acceder a las funciones de la tableta
import sensor, time, image, pyb

# Reiniciar el sensor para utilizarlo en un modo conocido
sensor.reset()

# Ajustar contraste y puntos de corte
sensor.set_contrast(2)
sensor.set_gainceiling(16)
# HQVGA y GRAYSCALE son las resoluciones que funcionan mejor
sensor.set_framesize(sensor.HQVGA)
sensor.set_pixformat(sensor.GRAYSCALE)

# Utilizar los modelos de detección de imagenes de cascada
# para la detección de rostros
face_cascade = image.HaarCascade("frontalface", stages=25)
print(face_cascade)

# Llevar el registro de clock para calcular FPS
clock = time.clock()

# El LED 2 es el de color verde
led1 = pyb.LED(2)

# Los LED IR permiten utilizar la detección incluso
# en áreas sin luz visible.
ir_led = pyb.LED(4)
ir_led.on()

# Utilizamos el puerto serie para enviar los conteos
# de detección
uart = pyb.UART(3, 115200, timeout_char=1000)

# Utilizamos el P4 para marcar y detectar cuando hay
# Una persona enfrente del sensor
detect_out = pyb.Pin(pyb.Pin.board.P4, pyb.Pin.OUT)

# En esta variable almacenaremos la cantidad
# de humanos frente al sensor
humans_detected = 0

while (True):
    clock.tick()

    # Capturar una imagen
    img = sensor.snapshot()

    # Ejecutar el algoritmo de detección de caras
    objects = img.find_features(face_cascade, threshold=0.75, scale_factor=1.25)

    # Nota: Esto solo es visible cuando
    # la OpenMV está conectada a la computadora
    # dibuja un recuadro sobre cada cara
    for r in objects:
        img.draw_rectangle(r,color=(255,0,0))

    # El número de objetos detectados equivale
    # al número de caras
    people = len(objects)

    # Habilitar LED y línea de detección
    # si el número de caras detectadas es
    # mayor a 0
    if people > 0:
        led1.on()
        detect_out.value(1);
    else:
        led1.off()
        detect_out.value(0);

    # Enviamos un mensaje con la cantidad de
    # caras solo si es diferente al número
    # anterior, para evitar saturar la salida
    if(people!=humans_detected):
        uart.write(str(people)+"\n")
        humans_detected = people
